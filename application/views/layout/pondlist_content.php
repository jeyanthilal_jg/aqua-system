<div id="right-panel" class="right-panel">

<?php $this->load->view('./include/top_menu'); ?>

<div class="breadcrumbs">
<div class="col-sm-4">
  <div class="page-header float-left">
    <div class="page-title">
      <h1>Pond List</h1>
    </div>
  </div>
</div>
<div class="col-sm-8">
  <div class="page-header float-right">
    <div class="page-title">
      <ol class="breadcrumb text-right">
        <li><a href="#">Dashboard</a></li>
        <li class="active">Pond List</li>
      </ol>
    </div>
  </div>
</div>
</div>

<div class="content mt-3">
<div class="animated fadeIn">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header"> <strong class="card-title">Pond List</strong> </div>
        <div class="card-body">
          <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Pond name</th>
                <th>Height</th>
                <th>Weight</th>
                <th>Depth</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                <td>depth</td>
                <td>date</td>
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                <td>depth</td>
                <td>date</td>
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                <td>depth</td>
                <td>date</td>
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                <td>depth</td>
                <td>date</td>
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                <td>depth</td>
                <td>date</td>
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                <td>depth</td>
                <td>date</td>
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header"> <strong class="card-title">Add Pond</strong> </div>
        <div class="card-body">
          <form name="pondlist" id="pondlist-form" method="post" action="<?php echo base_url('pondlist/create');?>">
					<?php if($this->session->flashdata('success')) { ?>
                       <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
                                <?php echo $this->session->flashdata('success');  ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              </div> 
                        <?php } ?>
                        <?php if($this->session->flashdata('failed')) {
                      ?>
                       <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
                                <?php echo $this->session->flashdata('failed');  ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              </div> 
                        <?php } ?>                 
                        
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="pondname">Pond name <span class="error">*</span></label>
                <input type="text" class="form-control" name="pondname" id="pondname" placeholder="Pond name">
              </div>
              <div class="form-group col-md-6">
                <label for="Width">Width in feet <span class="error">*</span></label>
                    <select name="width" id="width" class="form-control">
                    <option value="" selected>Choose...</option>
					<?php
                    for ($x = 0.5; $x <= 10; $x+=0.5) {
                    echo "<option value=".$x.">".$x." f</option>";
                    }
                    ?>                     
                    </select>
              </div>
              <div class="form-group col-md-6">
                <label for="height">Height in feet <span class="error">*</span></label>
                    <select name="height" id="height" class="form-control">
                    <option value="" selected>Choose...</option>
					<?php
                    for ($x = 0.5; $x <= 10; $x+=0.5) {
                    echo "<option value=".$x.">".$x." f</option>";
                    }
                    ?>                     
                    </select>
              </div>
              <div class="form-group col-md-6">
                <label for="depth">Depth in feet <span class="error">*</span></label>
                    <select name="depth" id="depth" class="form-control">
                    <option value="" selected>Choose...</option>
					<?php
                    for ($x = 0.5; $x <= 10; $x+=0.5) {
                    echo "<option value=".$x.">".$x." f</option>";
                    }
                    ?>                     
                    </select>
              </div>
            </div>
            <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputCity">Built date</label>
                    <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                      <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="submit" class="btn btn-secondary">Cancel</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- .animated --> 
</div>

</div><!-- /#right-panel -->

<!-- Right Panel -->
