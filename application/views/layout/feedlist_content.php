<div id="right-panel" class="right-panel">

<?php $this->load->view('./include/top_menu'); ?>

<div class="breadcrumbs">
<div class="col-sm-4">
  <div class="page-header float-left">
    <div class="page-title">
      <h1>Feed</h1>
    </div>
  </div>
</div>
<div class="col-sm-8">
  <div class="page-header float-right">
    <div class="page-title">
      <ol class="breadcrumb text-right">
        <li><a href="#">Dashboard</a></li>
        <li class="active">Feed List</li>
      </ol>
    </div>
  </div>
</div>
</div>

<div class="content mt-3">
<div class="animated fadeIn">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header"> <strong class="card-title">Feed List</strong> </div>
        <div class="card-body">
          <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Feed Name</th>
				<th>Feed Type</th>
				<th>Size</th>
				<th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
			  
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
              <tr>
                <td>Sample name</td>
                <td>height</td>
                <td>weight</td>
                
                <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header"> <strong class="card-title">Add Pond</strong> </div>
        <div class="card-body">
          <form id="feed-form" action="<?php echo base_url('feed/add_record');?>" method="post">
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="feedname">Feed name</label>
                    <input type="text" class="form-control" name="feedname" placeholder="Feed name">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="feedtype">Feed type</label>
                    <select id="feedtype" class="form-control" name="feed_type">
                      <option selected hidden disabled>Choose Feed Type</option>
                      <option>Natural</option>
					  <option>Chemical</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="size">Size</label>
                    <select id="size" class="form-control" name="size">
                      <option selected hidden disabled>Choose Feed Size</option>
                      <option>0.5</option>
					  <option>1.0</option>
					  <option>1.5</option>
					  <option>2.0</option>
					  <option>2.5</option>
					  <option>3.0</option>
					  <option>3.5</option>
					  <option>4.0</option>
					  <option>4.5</option>
					  <option>5.0</option>
					  <option>5.5</option>
					  <option>6.0</option>
					  <option>6.5</option>
					  <option>7.0</option>
					  <option>7.5</option>
                    </select>
                  </div>
                
				</div>
            <button type="submit" class="btn btn-primary" >Save</button>
            <button type="reset" class="btn btn-secondary">Cancel</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- .animated --> 
</div>

</div><!-- /#right-panel -->

<!-- Right Panel -->