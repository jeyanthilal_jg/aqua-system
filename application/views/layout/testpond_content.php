<style type="text/css">
div.dataTables_wrapper div.dataTables_length label {
   font-weight: normal;
   text-align: left;
   white-space: nowrap;
}
div.dataTables_wrapper div.dataTables_filter {
   text-align: right;
}
div.dataTables_wrapper div.dataTables_filter label {
   font-weight: normal;
   white-space: nowrap;
   text-align: left;
}
label {
   display: inline-block;
   margin-bottom: .5rem;
}
div.dataTables_wrapper div.dataTables_filter input {
   margin-left: 0.5em;
   display: inline-block;
   width: auto;
}
div.dataTables_wrapper div.dataTables_paginate {
   margin: 0;
   white-space: nowrap;
   text-align: right;
}
div.dataTables_wrapper div.dataTables_paginate ul.pagination {
   margin: 2px 0;
   white-space: nowrap;
   justify-content: flex-end;
}
div.dataTables_wrapper div.dataTables_length select {
   width: 75px;
   display: inline-block
}
</style>

<div id="right-panel" class="right-panel">

<?php $this->load->view('./include/top_menu'); ?>

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Test Pond</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Test Pond</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

     <div class="content mt-3">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title">Pond List</strong> </div>
            <div class="card-body">
			 
             <div id="bootstrap-data-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
				   <div class="row">
					  <div class="col-sm-12 col-md-6">
						 <div class="dataTables_length" id="bootstrap-data-table_length">
							<label>
							   Show 
							   <select name="per_page" aria-controls="bootstrap-data-table" class="form-control form-control-sm">
								  <option value="10">10</option>
								  <option value="20">20</option>
								  <option value="50">50</option>
								  <option value="-1">All</option>
							   </select>
							   entries
							</label>
						 </div>
					  </div>
					  <div class="col-sm-12 col-md-6">
						 <div id="bootstrap-data-table_filter" class="dataTables_filter"><label>Search:<input type="search" name="search" class="form-control form-control-sm" placeholder="" ></label></div>
						 <input type="hidden" name="sort_fld" value="">
						 <input type="hidden" name="sort_dir" value="">
					  </div>
				   </div>
							   
			   <div class="row">
				  <div class="col-sm-12">
					 <table class="table table-striped table-bordered no-footer" role="grid">
						<thead>
						   <tr role="row">
							  <th>Pond name</th>
							  <th>Height</th>
							  <th>width</th>
							  <th>Depth</th>
							  <th>Date</th>
							  <th>Action</th>
						   </tr>
						</thead>
						<tbody>
						   
						   <?php foreach($result_set as $row): 
							  $source = $row["_source"];
						   ?>
						     <tr role="row">
							  <td class=""><?php echo $source["pondname"]; ?></td>
							  <td><?php echo $source["height"]; ?></td>
							  <td><?php echo $source["width"]; ?></td>
							  <td><?php echo $source["depth"]; ?></td>
							  <td class=""><?php echo $source["built_date"]; ?></td>
							  <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
						    </tr>
						   <?php endforeach; ?>
						</tbody>
					 </table>
				  </div>
			   </div>
			   <div class="row">
				  <div class="col-sm-12 col-md-5">
					 <div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>
				  </div>
				  <div class="col-sm-12 col-md-7">
					 <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate">
					    <?php echo $page_links; ?>
						<!--<ul class="pagination">
						   <li class="paginate_button page-item previous" id="bootstrap-data-table_previous"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
						   <li class="paginate_button page-item active"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
						   <li class="paginate_button page-item"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
						   <li class="paginate_button page-item "><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
						   <li class="paginate_button page-item next" id="bootstrap-data-table_next"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="4" tabindex="0" class="page-link">Next</a></li>
						</ul> -->
					 </div>
				  </div>
			   </div>
			</div>			 
			 
			</div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title">Add Pond</strong> </div>
            <div class="card-body">
              <form>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="pondname">Pond name</label>
                    <input type="text" class="form-control" id="pondname" placeholder="Pond name">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="weight">Weight</label>
                    <input type="text" class="form-control" id="weight" placeholder="Weight">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="height">Height</label>
                    <input type="text" class="form-control" id="height" placeholder="Height">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="depth">Depth</label>
                    <input type="text" class="form-control" id="depth" placeholder="Count">
                  </div>
                </div>
                <button type="submit" class="btn btn-primary">Sign in</button>
                <button type="submit" class="btn btn-secondary">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .animated --> 
  </div>
  <!-- .content --> 
	
	
</div><!-- /#right-panel -->

<!-- Right Panel -->