<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pondlist extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
  public function __construct(){
  parent::__construct();
  $this->load->helper('date');
  $this->table="ponds";
 }

	public function index()
	{
		if($this->fb_rest->isloggedin()){
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
			$this->load->view('layout/pondlist_content');
			$this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}
	
	function create(){
		$table_name=$this->table;
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();		
		$result = $this->fb_rest->create_record($table_name,$form_data);
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('success','Record Created');
		redirect('/pondlist');
		}else{
		$this->session->set_flashdata('failed','Insert failed, Try Again!');
		redirect('/pondlist');
		}
	}
	
	
}
