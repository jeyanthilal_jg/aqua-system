<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feed extends CI_Controller {
	
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('date');
	}
	
	public function index()
	{
		if($this->fb_rest->isloggedin()){
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
			$this->load->view('layout/feedlist_content');
			$this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}
	
	public function add_record(){
		$form_data = $this->input->post();
		
		$feedname = $this->input->post("feedname");
		$feed_type = $this->input->post("feed_type");
		$size = $this->input->post("size");
		
		$str=$feedname;
		//echo $str;
		if($str!=0 || $str<=25)
		{
			$table_name = "feeds";
			$idata = array("feedname" => $feedname, 
			"size" => $size, 
			"feed_type" => $feed_type,
			"createdtime" => now(), 
			"updatedtime" => now());
			
			$result = $this->fb_rest->create_record($table_name, $idata);
			 fb_pr($result);
				if($result['status']=="success"){
				redirect('/feed/list_record');
			}
		}/* else{
			echo "<script type='text/javascript'>alert('Pleas Enter Valid Feed Name')</script>";
			asdf
			redirect('/feed');
		} */
	}
	
	public function list_record(){
		$table_name = "feeds";
		$this->fb_rest->test_list_record($table_name);
	}
}
	?>