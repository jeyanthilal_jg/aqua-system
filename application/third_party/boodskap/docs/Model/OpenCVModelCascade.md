# OpenCVModelCascade

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **string** |  | 
**description** | **string** |  | [optional] 
**color** | **string** |  | [optional] 
**min_size** | **string** |  | [optional] 
**max_size** | **string** |  | [optional] 
**scale_factor** | **double** |  | [optional] 
**min_neighbours** | **int** |  | [optional] 
**flags** | **int** |  | [optional] 
**font_face** | **int** |  | [optional] 
**font_scale** | **double** |  | [optional] 
**min_score** | **double** |  | [optional] 
**overlay_text** | **bool** |  | [optional] 
**overlay_type** | **string** |  | [optional] 
**cascade_size** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


