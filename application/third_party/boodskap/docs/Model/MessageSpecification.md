# MessageSpecification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**name** | **string** |  | 
**label** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**fields** | [**\Swagger\Client\Model\MessageField[]**](MessageField.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


