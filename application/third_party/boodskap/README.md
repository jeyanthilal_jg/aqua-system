# SwaggerClient-php
Boodskap IoT Platform (Domain API)

This PHP package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 1.0.6
- Build package: io.swagger.codegen.languages.PhpClientCodegen

## Requirements

PHP 5.4.0 and later

## Installation & Usage
### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```
{
  "repositories": [
    {
      "type": "git",
      "url": "https://github.com//.git"
    }
  ],
  "require": {
    "/": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
    require_once('/path/to/SwaggerClient-php/autoload.php');
```

## Tests

To run the unit tests:

```
composer install
./vendor/bin/phpunit
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\CreateUpdateDomainPropertyApi();
$atoken = "atoken_example"; // string | Auth token of the logged in domain
$entity = new \Swagger\Client\Model\DomainProperty(); // \Swagger\Client\Model\DomainProperty | DomainProperty JSON object

try {
    $result = $api_instance->storeDomainProperty($atoken, $entity);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreateUpdateDomainPropertyApi->storeDomainProperty: ', $e->getMessage(), PHP_EOL;
}

?>
```

## Documentation for API Endpoints

All URIs are relative to *https://api.boodskap.io*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*CreateUpdateDomainPropertyApi* | [**storeDomainProperty**](docs/Api/CreateUpdateDomainPropertyApi.md#storedomainproperty) | **POST** /domain/property/upsert/{atoken} | Create / Update Domain Property
*DeleteAllStoredDomainPropertiesApi* | [**deleteAllDomainProperties**](docs/Api/DeleteAllStoredDomainPropertiesApi.md#deletealldomainproperties) | **DELETE** /domain/property/deleteall/{atoken} | Delete All Stored Domain Properties
*DeleteStoredDomainPropertyApi* | [**deleteDomainProperty**](docs/Api/DeleteStoredDomainPropertyApi.md#deletedomainproperty) | **DELETE** /domain/property/delete/{atoken}/{name} | Delete Stored Domain Property
*ListDomainPropertiesApi* | [**listDomainProperties**](docs/Api/ListDomainPropertiesApi.md#listdomainproperties) | **GET** /domain/property/list/{atoken}/{pageSize} | List Domain Properties
*LoginApi* | [**login**](docs/Api/LoginApi.md#login) | **GET** /domain/login/{email}/{passwd} | Login
*LogoutApi* | [**logout**](docs/Api/LogoutApi.md#logout) | **GET** /domain/logout/{atoken} | Logout
*RetreiveDomainPropertyApi* | [**getDomainProperty**](docs/Api/RetreiveDomainPropertyApi.md#getdomainproperty) | **GET** /domain/property/get/{atoken}/{name} | Retreive Domain Property


## Documentation For Models

 - [ApiError](docs/Model/ApiError.md)
 - [Asset](docs/Model/Asset.md)
 - [AssetGroup](docs/Model/AssetGroup.md)
 - [AssetGroupProperty](docs/Model/AssetGroupProperty.md)
 - [AssetProperty](docs/Model/AssetProperty.md)
 - [BroadcastCommand](docs/Model/BroadcastCommand.md)
 - [ClusterNode](docs/Model/ClusterNode.md)
 - [Command](docs/Model/Command.md)
 - [CommandDefinition](docs/Model/CommandDefinition.md)
 - [CommandField](docs/Model/CommandField.md)
 - [CommandStatus](docs/Model/CommandStatus.md)
 - [Count](docs/Model/Count.md)
 - [Counter](docs/Model/Counter.md)
 - [Device](docs/Model/Device.md)
 - [DeviceGroup](docs/Model/DeviceGroup.md)
 - [DeviceGroupProperty](docs/Model/DeviceGroupProperty.md)
 - [DeviceMessage](docs/Model/DeviceMessage.md)
 - [DeviceModel](docs/Model/DeviceModel.md)
 - [DeviceModelProperty](docs/Model/DeviceModelProperty.md)
 - [DeviceProperty](docs/Model/DeviceProperty.md)
 - [Domain](docs/Model/Domain.md)
 - [DomainAssetGroup](docs/Model/DomainAssetGroup.md)
 - [DomainAssetGroupProperty](docs/Model/DomainAssetGroupProperty.md)
 - [DomainCounter](docs/Model/DomainCounter.md)
 - [DomainDeviceGroup](docs/Model/DomainDeviceGroup.md)
 - [DomainDeviceGroupProperty](docs/Model/DomainDeviceGroupProperty.md)
 - [DomainLicense](docs/Model/DomainLicense.md)
 - [DomainProperty](docs/Model/DomainProperty.md)
 - [DomainRule](docs/Model/DomainRule.md)
 - [DomainUserGroup](docs/Model/DomainUserGroup.md)
 - [DomainUserGroupProperty](docs/Model/DomainUserGroupProperty.md)
 - [EmailGateway](docs/Model/EmailGateway.md)
 - [Event](docs/Model/Event.md)
 - [EventRegistration](docs/Model/EventRegistration.md)
 - [FCMDevice](docs/Model/FCMDevice.md)
 - [FCMGateway](docs/Model/FCMGateway.md)
 - [FRImageList](docs/Model/FRImageList.md)
 - [FRLabelInfo](docs/Model/FRLabelInfo.md)
 - [FRLabelList](docs/Model/FRLabelList.md)
 - [FRModel](docs/Model/FRModel.md)
 - [FRModelInfo](docs/Model/FRModelInfo.md)
 - [FRModelList](docs/Model/FRModelList.md)
 - [FRPredict](docs/Model/FRPredict.md)
 - [FRPredictList](docs/Model/FRPredictList.md)
 - [Firmware](docs/Model/Firmware.md)
 - [FirmwareInfo](docs/Model/FirmwareInfo.md)
 - [GDResult](docs/Model/GDResult.md)
 - [Geofence](docs/Model/Geofence.md)
 - [GeofenceProperty](docs/Model/GeofenceProperty.md)
 - [GlobalData](docs/Model/GlobalData.md)
 - [GoogleMaps](docs/Model/GoogleMaps.md)
 - [InsertResult](docs/Model/InsertResult.md)
 - [LinkedDomain](docs/Model/LinkedDomain.md)
 - [Location](docs/Model/Location.md)
 - [Lookup](docs/Model/Lookup.md)
 - [LookupDelete](docs/Model/LookupDelete.md)
 - [LookupGet](docs/Model/LookupGet.md)
 - [LookupResult](docs/Model/LookupResult.md)
 - [MLAttribute](docs/Model/MLAttribute.md)
 - [MLData](docs/Model/MLData.md)
 - [MLDataList](docs/Model/MLDataList.md)
 - [MLModel](docs/Model/MLModel.md)
 - [MLModelInfo](docs/Model/MLModelInfo.md)
 - [MLModelList](docs/Model/MLModelList.md)
 - [MessageField](docs/Model/MessageField.md)
 - [MessageHeader](docs/Model/MessageHeader.md)
 - [MessageORSetting](docs/Model/MessageORSetting.md)
 - [MessageRule](docs/Model/MessageRule.md)
 - [MessageSpecification](docs/Model/MessageSpecification.md)
 - [NamedRule](docs/Model/NamedRule.md)
 - [NodeProperty](docs/Model/NodeProperty.md)
 - [OTABatch](docs/Model/OTABatch.md)
 - [OTABatchMember](docs/Model/OTABatchMember.md)
 - [OTADeviceBatch](docs/Model/OTADeviceBatch.md)
 - [OTADeviceBatchMember](docs/Model/OTADeviceBatchMember.md)
 - [OTAModelBatch](docs/Model/OTAModelBatch.md)
 - [OTAModelBatchMember](docs/Model/OTAModelBatchMember.md)
 - [OpenCVModel](docs/Model/OpenCVModel.md)
 - [OpenCVModelCascade](docs/Model/OpenCVModelCascade.md)
 - [OpenCVModelCount](docs/Model/OpenCVModelCount.md)
 - [OpenCVORLabel](docs/Model/OpenCVORLabel.md)
 - [OpenCVPredictResult](docs/Model/OpenCVPredictResult.md)
 - [Param](docs/Model/Param.md)
 - [Property](docs/Model/Property.md)
 - [PropertyCommand](docs/Model/PropertyCommand.md)
 - [PushMessage](docs/Model/PushMessage.md)
 - [PushResult](docs/Model/PushResult.md)
 - [RecordDefinition](docs/Model/RecordDefinition.md)
 - [RecordField](docs/Model/RecordField.md)
 - [Rule](docs/Model/Rule.md)
 - [ScheduledRule](docs/Model/ScheduledRule.md)
 - [Script](docs/Model/Script.md)
 - [ScriptResult](docs/Model/ScriptResult.md)
 - [SearchQuery](docs/Model/SearchQuery.md)
 - [SearchResult](docs/Model/SearchResult.md)
 - [Storable](docs/Model/Storable.md)
 - [Success](docs/Model/Success.md)
 - [Template](docs/Model/Template.md)
 - [TemplateQuery](docs/Model/TemplateQuery.md)
 - [TensorFlowModel](docs/Model/TensorFlowModel.md)
 - [TensorFlowORLabel](docs/Model/TensorFlowORLabel.md)
 - [TensorFlowPredictResult](docs/Model/TensorFlowPredictResult.md)
 - [TwilioGateway](docs/Model/TwilioGateway.md)
 - [UDPGateway](docs/Model/UDPGateway.md)
 - [User](docs/Model/User.md)
 - [UserDomain](docs/Model/UserDomain.md)
 - [UserGroup](docs/Model/UserGroup.md)
 - [UserGroupProperty](docs/Model/UserGroupProperty.md)
 - [UserProperty](docs/Model/UserProperty.md)


## Documentation For Authorization

 All endpoints do not require authorization.


## Author




