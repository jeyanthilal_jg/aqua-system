$.noConflict();

jQuery(document).ready(function($) {

	"use strict";

	[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
		new SelectFx(el);
	} );

	jQuery('.selectpicker').selectpicker;


	$('#menuToggle').on('click', function(event) {
		$('body').toggleClass('open');
	});

	$('.search-trigger').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').addClass('open');
	});

	$('.search-close').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').removeClass('open');
	});
	
	jQuery.validator.addMethod("validate_email", function(value, element) {
	if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
		return true;
	} else {
			return false;
		}
	}, "Please enter a valid Email.");

	$("#login-form").validate({
		rules: {
			username: {
				required: true,
				validate_email: true
			},
			password: {
				required: true,
			},
			
		},
		messages: {

			username: {
				required: "Please enter a username",
				email: "Please enter a valid email address",
			},
			password: {
				required: "Please provide a password",
			},
	}
	});
	
	$("#pondlist-form").validate({
		rules: {
			pondname: {
				required: true,
			},
			height: {
				required: true,
			},
			width: {
				required: true,
			},
			depth: {
				required: true,
			},
		},
	});
	
	$("#feed-form").validate({
		rules: {
			feedname: {
				required: true
			},
			feed_type:{
				required: true
			},
			size:{
				required: true
			}
			
		}
		
	});
	
	$(document).on( "mousemove", function( event ) {
	  var dw = $(document).width() / 15;
	  var dh = $(document).height() / 15;
	  var x = event.pageX/ dw;
	  var y = event.pageY/ dh;
	  $('.eye-ball').css({
		width : x,
		height : y
	  });
	});


});